package com.example.examplemod;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class KeyEventHandler {

    boolean toggle = false;

    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {

        System.out.println("Toggle = " + toggle);

        if(KeyBindings.keyBindings[0].isPressed() && !toggle) {
            toggle = true;
            System.out.println("Toggled on");
        }

        else if(KeyBindings.keyBindings[0].isPressed() && toggle) {
            toggle = false;
            System.out.println("Toggled off");
        }
    }

}