package com.example.examplemod;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Main.MODID, version = Main.VERSION)
public class Main {
    public static final String MODID = "examplemod";
    public static final String VERSION = "1.0";

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) throws Exception{

        FMLCommonHandler.instance().bus().register(new KeyEventHandler());
        KeyBindings.init();
        SpeechRecognizer.init();
    }

    @EventHandler
    public void load(FMLInitializationEvent event) throws Exception{
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) throws Exception {
    }
}
