package com.example.examplemod;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import org.lwjgl.input.Keyboard;

public class KeyBindings {

    public static KeyBinding[] keyBindings;

    public static void init() {

        // declare an array of key bindings
        keyBindings = new KeyBinding[1];

        // instantiate the key bindings
        keyBindings[0] = new KeyBinding("Toggle Voice Recognition", Keyboard.KEY_GRAVE, "CS582: Semester Project");

        // register all the key bindings
        for (int i = 0; i < keyBindings.length; ++i) {
            ClientRegistry.registerKeyBinding(keyBindings[i]);
        }
    }

}