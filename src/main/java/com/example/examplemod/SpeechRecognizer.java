package com.example.examplemod;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;

/**
 * Created by trentedilloran on 4/28/16.
 */
public class SpeechRecognizer {

    private static final String ACOUSTIC_MODEL_PATH = "resource:/edu/cmu/sphinx/models/en-us/en-us";
    private static final String DICTIONARY_PATH = "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict";
    private static final String GRAMMAR_PATH = "/Users/trentedilloran/IdeaProjects/forge/src/main/resources";

    // Initializes speech recognition configuration
    public static void init() throws Exception {
        Configuration configuration = new Configuration();
        configuration.setAcousticModelPath(ACOUSTIC_MODEL_PATH);
        configuration.setDictionaryPath(DICTIONARY_PATH);
        configuration.setGrammarPath(GRAMMAR_PATH);
        configuration.setUseGrammar(true);
        configuration.setGrammarName("myGrammar");
        LiveSpeechRecognizer myLiveSpeechRecognizer = new LiveSpeechRecognizer(configuration);
    }

    // Starts voice recognition
    public void start() {

    }

    // Stops voice recognition
    public void stop() {

    }
}
